from functools import cached_property
import xgitlab


class Action:
    def __init__(
        self,
        resource: xgitlab.Resource,
        who: xgitlab.User,
        weight: int,
        why: str,
        uid: str,
    ):
        self.resource = resource
        # who is responsible for performing the action
        self.who = who
        self.weight = weight
        self.why = why
        self.uid = uid

    @cached_property
    def web_url(self) -> str:
        return self.resource.web_url


class MRAction(Action):
    def __init__(
        self,
        mr: xgitlab.MergeRequest,
        who: xgitlab.User,
        weight: int,
        why: str,
        uid: str,
    ):
        if mr.first_contribution:
            weight += 50
        super().__init__(mr, who, weight, why, uid)


class IssueAction(Action):
    def __init__(
        self,
        issue: xgitlab.Issue,
        who: xgitlab.User,
        weight: int,
        why: str,
        uid: str,
    ):
        super().__init__(issue, who, weight, why, uid)


class Merge(MRAction):
    printable = 'Merge'
    description = 'MR is ready to be merged and you have merge power'

    def __init__(self, mr: xgitlab.MergeRequest, who: xgitlab.User, why: str):
        super().__init__(
            mr, who, weight=2100, why=why, uid=f"{mr.uid}.Merge.{mr.base_uid}",
        )


class Rebase(MRAction):
    printable = 'Rebase'
    description = 'MR is ready but needs automatic rebase before being merged'

    def __init__(self, mr: xgitlab.MergeRequest, who: xgitlab.User, why: str):
        super().__init__(
            mr,
            who,
            weight=2000,
            why=why,
            uid=f"{mr.uid}.Rebase.{mr.base_uid}",
        )


class ManualRebase(MRAction):
    printable = 'Rebase manually'
    description = 'MR is ready but needs manual rebase'

    def __init__(self, mr: xgitlab.MergeRequest, who: xgitlab.User, why: str):
        super().__init__(
            mr,
            who,
            weight=1900,
            why=why,
            uid=f"{mr.uid}.ManualRebase.{mr.base_uid}",
        )


class CloseThreadOrReply(MRAction):
    printable = 'Close or reply to thread'
    description = "The author has responded to a thread in which you've participated, please reply or close the thread"

    def __init__(
        self,
        mr: xgitlab.MergeRequest,
        who: xgitlab.User,
        comment: xgitlab.Comment,
    ):
        super().__init__(
            mr,
            who,
            weight=1800,
            why='is the thread author',
            uid=f"{mr.uid}.CloseThreadOrReply.{comment.uid}",
        )
        self.comment = comment

    @cached_property
    def web_url(self) -> str:
        return self.comment.web_url


class ReplyToThread(MRAction):
    printable = 'Reply to thread'
    description = "A thread is open on your MR to which you've not replied, please reply."

    def __init__(
        self,
        mr: xgitlab.MergeRequest,
        who: xgitlab.User,
        why: str,
        comment: xgitlab.Comment,
    ):
        super().__init__(
            mr,
            who,
            weight=1700,
            why=why,
            uid=f"{mr.uid}.ReplyToThread.{comment.uid}",
        )
        self.comment = comment

    @cached_property
    def web_url(self) -> str:
        return self.comment.web_url


class ReviewOrApprove(MRAction):
    printable = 'Review or approve'
    description = "This MR requires a review or an approve, and you're assigned as a reviewer"

    def __init__(self, mr: xgitlab.MergeRequest, who: xgitlab.User, why: str):
        super().__init__(
            mr, who, weight=1600, why=why, uid=f"{mr.uid}.ReviewOrApprove",
        )


class FixCI(MRAction):
    printable = 'Fix CI'
    description = 'The latest pipeline of this MR failed, please fix'

    def __init__(
        self,
        mr: xgitlab.MergeRequest,
        who: xgitlab.User,
        why: str,
        pipeline: dict,
    ):
        super().__init__(
            mr,
            who,
            weight=1500,
            why=why,
            uid=f"{mr.uid}.FixCI.{pipeline['id']}",
        )
        self.pipeline = pipeline

    @cached_property
    def web_url(self) -> str:
        return self.pipeline['web_url']


class CompleteTask(MRAction):
    printable = 'Complete task'
    description = 'This MR has uncompleted tasks'

    def __init__(self, mr: xgitlab.MergeRequest, who: xgitlab.User, why: str):
        super().__init__(
            mr,
            who,
            weight=1400,
            why=why,
            uid=f"{mr.uid}.CompleteTask.{mr.task_completion_uid}",
        )


class AddMTReviewers(MRAction):
    printable = 'Add a merge-team reviewer'

    def __init__(self, mr: xgitlab.MergeRequest, who: xgitlab.User, why: str):
        super().__init__(
            mr, who, weight=1300, why=why, uid=f"{mr.uid}.AddMTReviewers",
        )
        # suggest MT members?
        self.description = f'This MR lacks merge-team reviewers, please add one or refer to merge-dispatchers {mr.repo.printable_dispatchers}.'


class AddReviewers(MRAction):
    printable = 'Add reviewers'

    def __init__(self, mr: xgitlab.MergeRequest, who: xgitlab.User, why: str):
        super().__init__(
            mr, who, weight=1200, why=why, uid=f"{mr.uid}.AddReviewers",
        )
        # suggest reviewers?
        self.description = f'This MR lacks reviewers, please add one or refer to merge-dispatchers {mr.repo.printable_dispatchers}.'


class AssignIssue(IssueAction):
    printable = 'Assign issue'
    description = 'This issue is open and unassigned'

    def __init__(self, issue: xgitlab.Issue, who: xgitlab.User, why: str):
        super().__init__(
            issue, who, weight=1100, why=why, uid=f"{issue.uid}.AssignIssue",
        )


class HelpAddMTReviewers(MRAction):
    printable = 'Help add merge-team reviewers'
    description = 'This MR lacks merge-team reviewers, please help to add one'

    def __init__(self, mr: xgitlab.MergeRequest, who: xgitlab.User):
        super().__init__(
            mr,
            who,
            weight=1000,
            why='is a dispatcher',
            uid=f"{mr.uid}.HelpAddMTReviewers",
        )


class HelpAddReviewers(MRAction):
    printable = 'Help add reviewers'
    description = 'This MR lacks reviewers, please help to add one'

    def __init__(self, mr: xgitlab.MergeRequest, who: xgitlab.User):
        super().__init__(
            mr,
            who,
            weight=900,
            why='is a dispatcher',
            uid=f"{mr.uid}.HelpAddReviewers",
        )


class UnWIP(MRAction):
    printable = 'UnWIP'
    description = 'This MR is in WIP state. To move it forward, mark as ready if applicable'

    def __init__(self, mr: xgitlab.MergeRequest, who: xgitlab.User, why: str):
        super().__init__(
            mr, who, weight=800, why=why, uid=f"{mr.uid}.UnWIP",
        )


class SolveIssue(IssueAction):
    printable = 'Solve issue'
    description = 'This issue is open'

    def __init__(self, issue: xgitlab.Issue, who: xgitlab.User, why: str):
        super().__init__(
            issue, who, weight=700, why=why, uid=f"{issue.uid}.SolveIssue",
        )


class UnknownAction(MRAction):
    printable = 'No idea what to do'
    description = 'Action collector could not determine an action'

    def __init__(self, mr: xgitlab.MergeRequest, who: xgitlab.User):
        super().__init__(
            mr,
            who,
            weight=300,
            why='is a dispatcher',
            uid=f"{mr.uid}.UnknownAction",
        )


class UnblockMR(MRAction):
    printable = 'Unblock'
    description = 'This MR is blocked'

    def __init__(self, mr: xgitlab.MergeRequest, who: xgitlab.User, why: str):
        super().__init__(
            mr, who, weight=200, why=why, uid=f"{mr.uid}.Unblock",
        )


class WaitForPipeline(MRAction):
    printable = 'Wait for pipeline'
    description = 'Pipeline is running'

    def __init__(self, mr: xgitlab.MergeRequest, who: xgitlab.User, why: str):
        super().__init__(
            mr, who, weight=100, why=why, uid=f"{mr.uid}.WaitForPipeline",
        )
