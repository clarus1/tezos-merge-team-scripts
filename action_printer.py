from os import path
from functools import cached_property
from typing import Dict, Iterable, List, Mapping, Tuple
import itertools
import action
import jinja2
import xgitlab

# Doc for
# Jinja: https://jinja.palletsprojects.com/en/2.11.x/templates
# Bootstrap: https://getbootstrap.com/docs/5.0/


def by_user(
    actions: Iterable[action.Action], extra_users: Iterable[xgitlab.User] = [],
) -> Dict[xgitlab.User, List[action.Action]]:
    users = {action.who for action in actions} | set(extra_users)
    return {
        user: [action for action in actions if action.who == user]
        for user in users
    }


def sort_actions(actions: Iterable[action.Action]) -> Iterable[action.Action]:
    def sort_by_weight(
        actions: Iterable[action.Action],
    ) -> Tuple[List[int], List[action.Action]]:
        by_weight = sorted(
            actions,
            key=lambda action: (
                -action.weight,
                action.uid,
                action.why,
                action.who,
            ),
        )
        weights = [action.weight for action in by_weight]
        return (weights, by_weight)

    def rc_key(action: action.Action) -> xgitlab.Resource:
        return action.resource

    by_resource = [
        sort_by_weight(actions)
        for _rc, actions in itertools.groupby(
            sorted(actions, key=rc_key), key=rc_key
        )
    ]
    by_rc_weight = sorted(
        by_resource,
        reverse=True,
        key=lambda weights_actions: weights_actions[0],
    )
    for _weights, actions in by_rc_weight:
        yield from actions


class Printer:
    @cached_property
    def env(self) -> jinja2.environment.Environment:
        return jinja2.Environment(
            loader=jinja2.FileSystemLoader('templates'),
            autoescape=jinja2.select_autoescape(['html', 'xml']),
            undefined=jinja2.StrictUndefined,
        )

    @cached_property
    def index_template(self) -> jinja2.environment.Template:
        return self.env.get_template('index.html')

    @cached_property
    def user_template(self) -> jinja2.environment.Template:
        return self.env.get_template('user.html')

    def print_index(
        self,
        output_dir: str,
        server: xgitlab.Server,
        actions: Iterable[action.Action],
        actions_by_user: Mapping[xgitlab.User, List[action.Action]],
    ) -> None:
        sorted_actions = list(sort_actions(actions))

        actions_counts = {
            user: len(actions) for user, actions in actions_by_user.items()
        }

        with open(path.join(output_dir, 'index.html'), 'w') as output:
            output.write(
                self.index_template.render(
                    repositories=server.registered_repositories,
                    sorted_actions=sorted_actions,
                    actions_counts=actions_counts,
                )
            )

    def print_user_actions(
        self,
        output_dir: str,
        user: xgitlab.User,
        user_actions: Iterable[action.Action],
    ) -> None:
        sorted_actions = list(sort_actions(user_actions))

        with open(
            path.join(output_dir, f'{user.username}.html'), 'w'
        ) as output:
            output.write(
                self.user_template.render(
                    user=user, sorted_actions=sorted_actions
                )
            )

    def print_all(
        self,
        output_dir: str,
        server: xgitlab.Server,
        actions: List[action.Action],
    ) -> None:
        actions_by_user = by_user(actions, extra_users=server.users.values())

        self.print_index(output_dir, server, actions, actions_by_user)

        for user, user_actions in actions_by_user.items():
            self.print_user_actions(output_dir, user, user_actions)
