import argparse
import os

import action_collector
import action_printer
import projects
import xgitlab


def parse_arguments():
    parser = argparse.ArgumentParser()
    parser.add_argument('--private-token', type=str, required=True)
    parser.add_argument(
        '--output-dir', type=str, default=".",
    )
    return parser.parse_args()


def main(args) -> None:
    if not os.path.isdir(args.output_dir):
        os.mkdir(args.output_dir)

    server = xgitlab.Server.default(
        projects=projects.PROJECTS, private_token=args.private_token
    )
    actions = list(action_collector.collect_actions_from_server(server))
    printer = action_printer.Printer()
    printer.print_all(args.output_dir, server, actions)


if __name__ == '__main__':
    main(parse_arguments())
