TYPECHECK=poetry run mypy
PYLINT=poetry run pylint
PYCODESTYLE=poetry run pycodestyle
BLACK=poetry run black
PACKAGES=.
LOG_DIR=tmp

lint: lint_pylint lint_pycodestyle lint_black typecheck

typecheck:
	@echo "Typechecking with mypy version `poetry run mypy --version`"
	$(TYPECHECK) *.py

lint_pylint:
	@echo "Linting with pylint, version:"
	@poetry run pylint --version | sed 's/^/  /'
	$(PYLINT) $(PACKAGES)

lint_pycodestyle:
	@echo "Linting with pycodestyle version `poetry run pycodestyle --version` (`poetry run which pycodestyle`)"
	$(PYCODESTYLE) $(PACKAGES)

lint_black:
	@echo "Checking format with black"
	$(BLACK) --check --line-length 79 $(PACKAGES)

black:
	@echo "Running the black formatter"
	$(BLACK)  --line-length 79 $(PACKAGES)

help:
	@grep '^.*:.*#.*' Makefile | grep -v grep | sed 's/\(.*:\) .*#\(.*\)/\1\2/'
