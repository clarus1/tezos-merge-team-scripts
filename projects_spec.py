import copy
import itertools
from abc import ABC
from typing import (
    Iterable,
    List as ty_List,
    Optional,
    Union,
    Sequence,
    Type,
    TypeVar,
)


class Ref:
    def __init__(self, id: Union[int, str]):
        self.id = id

    def _ref_attrs(self) -> ty_List[str]:
        return [f'id={self.id.__repr__()}']

    def __repr__(self) -> str:
        return f'{type(self).__name__}({self.id.__repr__()})'


class PeopleSpec(ABC):
    pass


class Default(PeopleSpec):
    def __repr__(self) -> str:
        return f'{type(self).__name__}'


class Issue(Ref, PeopleSpec):
    pass


class Users(PeopleSpec):
    def __init__(self, *usernames: str):
        self.usernames = usernames

    def __repr__(self) -> str:
        return f'{type(self).__name__}(usernames=[{", ".join([u.__repr__() for u in self.usernames])}])'


TProjectPeopleSpec = TypeVar('TProjectPeopleSpec', bound='ProjectPeopleSpec')


class ProjectPeopleSpec:
    def __init__(
        self,
        *,
        dispatchers: Optional[PeopleSpec] = None,
        issuewatchers: Optional[PeopleSpec] = None,
        mergeteamers: Optional[PeopleSpec] = None,
    ):
        self.dispatchers = dispatchers
        self.issuewatchers = issuewatchers
        self.mergeteamers = mergeteamers

    def inherited_from(
        self: TProjectPeopleSpec, parent: 'ProjectPeopleSpec'
    ) -> TProjectPeopleSpec:
        res = copy.copy(self)
        if res.dispatchers is None:
            res.dispatchers = parent.dispatchers
        if res.issuewatchers is None:
            res.issuewatchers = parent.issuewatchers
        if res.mergeteamers is None:
            res.mergeteamers = parent.mergeteamers
        return res

    def _people_attrs(self) -> ty_List[str]:
        attr_names = ['dispatchers', 'issuewatchers', 'mergeteamers']
        attrs = (
            (attr_name, getattr(self, attr_name, None))
            for attr_name in attr_names
        )
        return [
            f'{attr_name}={attr_val.__repr__()}'
            for (attr_name, attr_val) in attrs
            if attr_val is not None
        ]


EMPTY_PROJECT_PEOPLE_SPEC = ProjectPeopleSpec()


class ProjectRef(ProjectPeopleSpec, ABC):
    pass


class RefAndProjectRef(Ref, ProjectRef):
    def __init__(self, id: Union[int, str], **kw):
        Ref.__init__(self, id=id)
        ProjectRef.__init__(self, **kw)

    def __repr__(self) -> str:
        return f'{type(self).__name__}({", ".join(self._ref_attrs() + self._people_attrs())})'


class Project(RefAndProjectRef):
    pass


class Group(RefAndProjectRef):
    pass


class List(ProjectRef):
    def __init__(self, *items: ProjectRef, **kw):
        ProjectRef.__init__(self, **kw)
        self.items = items

    def __repr__(self) -> str:
        return f'{type(self).__name__}({", ".join([item.__repr__() for item in self.items] + self._people_attrs())})'


EMPTY_PROJECT_SPEC = List()
