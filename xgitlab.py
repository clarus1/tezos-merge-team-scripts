from functools import cached_property
from typing import (
    Callable,
    Dict,
    Iterable,
    List,
    NamedTuple,
    Optional,
    Sequence,
    Set,
    Tuple,
    Type,
    TypeVar,
    Union,
)
from datetime import datetime, timedelta
from gitlab import Gitlab  # type: ignore
import gitlab.v4.objects as gitlab_objects  # type: ignore
import re
import projects_spec

# Parsers and textual heuristics

RE_MENTION = re.compile(r'@(\w(?:[-.\w]|\\[-._])*)')
RE_PUSH = re.compile(r'added \d+ commit')
RE_BLOCKED = re.compile(
    r'^((?:Blocked|Depend).+)$', re.MULTILINE + re.IGNORECASE
)
RE_DEPENDENCY = re.compile(r'(?:\b\w[-\w]*/\w[-\w]*)?[!#]\d+\b')
RE_ISSUE = re.compile(r'^([^#]+)#(\d+)$')


def is_automatic_merge(text: str) -> bool:
    return text.startswith('enabled an automatic merge')


def is_blocked(text: str) -> bool:
    return RE_BLOCKED.match(text) is not None


def is_changed_line(text: str) -> bool:
    return text.startswith('changed this line')


def is_push(text: str) -> bool:
    return RE_PUSH.match(text) is not None


def normalize_mention(name: str) -> str:
    return name.replace('\\', '')


def get_mentioned_users(text: str) -> Set[str]:
    return set(normalize_mention(u) for u in RE_MENTION.findall(text))


def get_reviewers_section(text: str) -> str:
    i = text.lower().rfind('reviewer')
    return text[i:] if i >= 0 else ''


def parse_datetime(text: str) -> datetime:
    if text.endswith('Z'):
        text = text[:-1]
    return datetime.fromisoformat(text)


def get_repo_group(name: str) -> str:
    group, _slash, _repo = name.rpartition('/')
    assert group != ''
    return group


def parse_issue_ref(ref: str) -> Tuple[str, int]:
    m = RE_ISSUE.match(ref)
    assert m is not None
    repo, id = m.groups()
    return (repo, int(id))


# Classes


class Wrapper:
    def __init__(self, wrapped):
        self.wrapped = wrapped

    def __getattr__(self, attr):
        try:
            return getattr(self.wrapped, attr)
        except AttributeError as error:
            if attr == 'wrapped_more':
                raise error
            more = getattr(self, 'wrapped_more', None)
            if more is None:
                raise error
            else:
                self.wrapped = more
                return getattr(more, attr)


class User:
    avatar_url = None

    def __init__(
        self, _server: 'Server', username: str,
    ):
        self.username = username
        self.name = username
        self.web_url = f'https://gitlab.com/{username}'

    def update_from_dict(self, dic: dict) -> 'User':
        self.name = dic['name']
        self.avatar_url = dic['avatar_url']
        self.web_url = dic['web_url']
        return self

    def update_from_project_member(
        self, mem: gitlab_objects.ProjectMember
    ) -> 'User':
        self.name = mem.name
        self.avatar_url = mem.avatar_url
        self.web_url = mem.web_url
        return self

    def __lt__(self, other: 'User') -> bool:
        return self.username < other.username

    def __eq__(self, other: object) -> bool:
        return isinstance(other, User) and self.username == other.username

    def __hash__(self) -> int:
        return hash(self.username)

    @cached_property
    def printable(self) -> str:
        return self.username

    @cached_property
    def printable_name(self) -> str:
        return getattr(self, 'name', self.printable)

    @cached_property
    def avatar_alt(self) -> str:
        return f'@{self.username}'

    def __repr__(self) -> str:
        return f'{type(self).__name__}({self.username.__repr__()})'


TTempUser = TypeVar('TTempUser', bound='TempUser')


class TempUser(User):
    def __init__(self, username: str):
        self.username = username

    @classmethod
    def from_dict(cls: Type[TTempUser], dic: dict) -> TTempUser:
        return cls(dic['username'])


class Push(Wrapper):
    def __init__(self, wrapped: Union['Comment', 'MergeRequest']):
        super().__init__(wrapped)

    def __repr__(self) -> str:
        return f'{type(self).__name__}({self.wrapped.__repr__()})'


class Comment:
    def __init__(self, thread: 'Thread', note_dict: dict):
        self.thread = thread
        self.__attributes = note_dict

    @cached_property
    def uid(self) -> int:
        return self.__attributes['id']

    @cached_property
    def author(self) -> User:
        return self.thread.mr.repo.server.user_from_dict(
            self.__attributes['author']
        )

    @cached_property
    def system(self) -> bool:
        return self.__attributes['system']

    @cached_property
    def body(self) -> str:
        return self.__attributes['body']

    @cached_property
    def created_at(self) -> datetime:
        return parse_datetime(self.__attributes['created_at'])

    @cached_property
    def changed_line(self) -> bool:
        return self.system and is_changed_line(self.body)

    @cached_property
    def automatic_merger(self) -> Optional[User]:
        return (
            self.author
            if self.system and is_automatic_merge(self.body)
            else None
        )

    @cached_property
    def as_push(self) -> Optional[Push]:
        return Push(self) if self.system and is_push(self.body) else None

    @cached_property
    def web_url(self) -> str:
        return f"{self.thread.mr.web_url}#note_{self.__attributes['id']}"

    def __repr__(self) -> str:
        return f'{type(self).__name__}({self.uid.__repr__()} on {self.thread.__repr__()})'


class Thread(Wrapper):
    def __init__(
        self,
        mr: 'MergeRequest',
        discussion: gitlab_objects.ProjectMergeRequestDiscussion,
    ):
        super().__init__(discussion)
        self.mr = mr
        self.__notes = discussion.attributes['notes']

    @cached_property
    def unresolved_notes(self) -> List[dict]:
        return [
            n
            for n in self.__notes[::-1]
            if n['resolvable'] and not n['resolved']
        ]

    @cached_property
    def last_non_author_unresolved_comment(self) -> Optional[Comment]:
        for n in self.unresolved_notes:
            if TempUser.from_dict(n['author']) not in self.mr.authors:
                return Comment(self, n)
        return None

    @cached_property
    def unresolved_comments(self) -> Dict[User, Comment]:
        return {
            self.mr.repo.server.user_from_dict(n['author']): Comment(self, n)
            for n in self.unresolved_notes[::-1]
        }

    @cached_property
    def resolved(self) -> bool:
        return not any(self.unresolved_notes)

    @cached_property
    def last_comment(self) -> Comment:
        return Comment(self, self.__notes[-1])

    @cached_property
    def last_comment_is_from_author(self) -> bool:
        return self.last_comment.author in self.mr.authors

    @cached_property
    def line_has_been_updated(self) -> bool:
        return self.last_comment.changed_line

    @cached_property
    def last_comment_is_from_author_or_line_has_been_updated(self) -> bool:
        return self.last_comment_is_from_author or self.line_has_been_updated

    @cached_property
    def automatic_merger(self) -> Optional[User]:
        return (
            self.last_comment.automatic_merger
            if self.wrapped.individual_note
            else None
        )

    @cached_property
    def as_push(self) -> Optional[Push]:
        return (
            self.last_comment.as_push if self.wrapped.individual_note else None
        )

    def __repr__(self) -> str:
        return f'{type(self).__name__}({self.wrapped.id.__repr__()} on {self.mr.__repr__()})'


class Resource(Wrapper):
    def __init__(self, repo: 'Repository', resource):
        super().__init__(resource)
        self.repo = repo

    @cached_property
    def uid(self) -> str:
        return self.wrapped.id

    @cached_property
    def printable_id(self) -> str:
        return self.wrapped.references['full']

    def __lt__(self, other: 'Resource') -> bool:
        return self.printable_id < other.printable_id

    def __eq__(self, other: object) -> bool:
        return (
            isinstance(other, Resource)
            and self.printable_id == other.printable_id
        )

    @cached_property
    def public(self) -> bool:
        return self.wrapped.visibility == 'public'

    @cached_property
    def title(self) -> str:
        return (
            self.wrapped.title
            if self.public or self.repo.server.allow_private
            else self.printable_id
        )

    def __repr__(self) -> str:
        return f'{type(self).__name__}({self.printable_id.__repr__()})'


class MergeRequest(Resource):
    def __init__(
        self,
        repo: 'Repository',
        mr: Union[
            gitlab_objects.MergeRequest, gitlab_objects.ProjectMergeRequest
        ],
    ):
        super().__init__(repo, mr)

    @cached_property
    def wrapped_more(self) -> gitlab_objects.MergeRequest:
        return self.repo.mergerequests.get(
            self.wrapped.iid,
            include_diverged_commits_count=True,
            include_rebase_in_progress=True,
        )

    @cached_property
    def author(self) -> User:
        return self.repo.server.user_from_dict(self.wrapped.author)

    @cached_property
    def created_at(self) -> datetime:
        return parse_datetime(self.wrapped.attributes['created_at'])

    @cached_property
    def pushes(self) -> Sequence[Push]:
        first_push = Push(self)
        thread_pushes = [
            t.as_push for t in self.threads if t.as_push is not None
        ]
        return [first_push] + thread_pushes

    @cached_property
    def last_push(self) -> datetime:
        if any(self.pushes):
            return self.pushes[-1].created_at
        return parse_datetime(self.wrapped.created_at)

    @cached_property
    def authors(self) -> Dict[User, str]:
        last_author_date = self.last_push - timedelta(days=60)
        authors = {
            p.author: 'pushed recently'
            for p in self.pushes
            if p.created_at < last_author_date
        }
        if self.author in authors or not authors:
            authors[self.author] = 'is the original author'
        return authors

    @cached_property
    def merger(self) -> Optional[Tuple[User, str]]:
        if self.wrapped.merged_by is not None:
            return (
                self.repo.server.user_from_dict(self.wrapped.merged_by),
                'merged',
            )
        for t in self.threads[::-1]:
            if t.automatic_merger:
                return (t.automatic_merger, 'set automatic merge')
        return None

    @cached_property
    def authors_and_merger(self) -> Dict[User, str]:
        res = self.authors
        if not self.merger:
            return res
        merger_user, merger_why = self.merger
        if merger_user in res:
            merger_why = f'{res[merger_user]} and {merger_why}'
        return {**res, merger_user: merger_why}

    @cached_property
    def authors_and_pipeline_user(self) -> Dict[User, str]:
        res = self.authors
        head_pipeline_user = self.repo.server.user_from_dict(
            self.head_pipeline['user']
        )
        head_pipeline_why = 'triggered last pipeline'
        if head_pipeline_user in res:
            head_pipeline_why = (
                f'{res[head_pipeline_user]} and {head_pipeline_why}'
            )
        return {**res, head_pipeline_user: head_pipeline_why}

    @cached_property
    def blocked(self) -> bool:
        # TODO: currently no API to get dependent MRs/issues
        # TODO: check blockers are still open
        return is_blocked(self.wrapped.description)

    @cached_property
    def reviewers_in_description(self) -> Set[User]:
        return {
            self.repo.server.user_from_username(un)
            for un in get_mentioned_users(
                get_reviewers_section(self.wrapped.description)
            )
        }

    @cached_property
    def assignees(self) -> Set[User]:
        return {
            self.repo.server.user_from_dict(u) for u in self.wrapped.assignees
        }

    @cached_property
    def approvals(self) -> gitlab_objects.ProjectMergeRequestApproval:
        return self.wrapped.approvals.get()

    @cached_property
    def approvers(self) -> Set[User]:
        return {
            self.repo.server.user_from_dict(a['user'])
            for a in self.approvals.approved_by
        }

    @cached_property
    def set_reviewers(self) -> Set[User]:
        return {
            self.repo.server.user_from_dict(u) for u in self.wrapped.reviewers
        }

    @cached_property
    def all_reviewers(self) -> Set[User]:
        return (
            self.set_reviewers
            | self.reviewers_in_description
            | self.assignees
            | self.approvers
        )

    @cached_property
    def threads(self) -> Sequence[Thread]:
        return [
            Thread(self, d) for d in self.wrapped.discussions.list(all=True)
        ]

    @cached_property
    def unresolved_threads(self) -> List[Thread]:
        return [t for t in self.threads if not t.resolved]

    @cached_property
    def has_unresolved_threads(self) -> bool:
        return any(self.unresolved_threads)

    @cached_property
    def one_approval_left(self) -> bool:
        return self.approvals.approvals_left == 1

    @cached_property
    def has_at_least_one_approval(self) -> bool:
        return any(self.approvals.approved_by)

    @cached_property
    def has_uncompleted_tasks(self) -> bool:
        tcs = self.wrapped.task_completion_status
        return tcs['completed_count'] < tcs['count']

    @cached_property
    def task_completion_uid(self) -> str:
        tcs = self.wrapped.task_completion_status
        return f"{tcs['completed_count']}/{tcs['count']}"

    @cached_property
    def has_diverged(self) -> bool:
        return self.diverged_commits_count > 0

    @cached_property
    def non_approvers_reviewers(self) -> Dict[User, str]:
        return {
            **{
                u: 'is mentioned as reviewer'
                for u in self.reviewers_in_description - self.approvers
            },
            **{u: 'is assigned' for u in self.assignees - self.approvers},
            **{
                u: 'is set as reviewer'
                for u in self.set_reviewers - self.approvers
            },
        }

    @cached_property
    def mergeteam_reviewers(self) -> Dict[User, str]:
        return {
            u: f'is a merge-teamer and {w}'
            for u, w in self.non_approvers_reviewers.items()
            if u in self.repo.mergeteamers
        }

    @cached_property
    def mergers(self) -> Dict[User, str]:
        return (
            self.mergeteam_reviewers
            if self.mergeteam_reviewers
            else {
                u: f'is a dispatcher and there is no merge-team reviewer'
                for u in self.repo.dispatchers
            }
        )

    @cached_property
    def has_less_than_2_reviewers(self) -> bool:
        return len(self.all_reviewers) < 2

    @cached_property
    def has_no_non_dispatcher_mergeteamers_reviewers(self) -> bool:
        return not any(self.mergeteam_reviewers.keys() - self.repo.dispatchers)

    @cached_property
    def last_pipeline_status(self) -> str:
        return (
            self.head_pipeline['status']
            if self.head_pipeline is not None
            else 'none'
        )

    @cached_property
    def last_pipeline_failed(self) -> bool:
        return self.last_pipeline_status == 'failed'

    @cached_property
    def last_pipeline_succeeded(self) -> bool:
        return self.last_pipeline_status == 'success'

    @cached_property
    def pipelines(self) -> Iterable[dict]:
        return self.wrapped.pipelines()

    @cached_property
    def last_finished_pipeline_succeeded(self) -> bool:
        if self.last_pipeline_succeeded:
            return True
        for pipeline in self.pipelines:
            if pipeline['status'] == 'success':
                return True
            if pipeline['status'] == 'failed':
                return False
        return False

    @cached_property
    def base_uid(self) -> str:
        return self.diff_refs['base_sha']


class Issue(Resource):
    is_special = False

    def __init__(self, repo: 'Repository', issue: gitlab_objects.ProjectIssue):
        super().__init__(repo, issue)

    @cached_property
    def author(self) -> User:
        return self.repo.server.user_from_dict(self.wrapped.author)

    @cached_property
    def assignees(self) -> Set[User]:
        return {
            self.repo.server.user_from_dict(u) for u in self.wrapped.assignees
        }

    @cached_property
    def is_assigned(self) -> bool:
        return any(self.assignees)

    @cached_property
    def owners(self) -> Dict[User, str]:
        return (
            {u: 'is assigned' for u in self.assignees}
            if any(self.assignees)
            else {self.author: 'opened issue and no one is assigned'}
        )


class Repository(Wrapper):
    def __init__(
        self,
        server: 'Server',
        project: Union[gitlab_objects.Project, gitlab_objects.GroupProject],
    ):
        super().__init__(project)
        self.server = server
        self.registered = False
        self.project_people_spec = projects_spec.EMPTY_PROJECT_PEOPLE_SPEC
        self.__issues: Dict[int, Issue] = dict()

    @cached_property
    def wrapped_more(self) -> gitlab_objects.Project:
        return self.server.fetch_raw_project(self.wrapped.id)

    @cached_property
    def owners(self) -> Set[User]:
        members = list(self.members.all(all=True))
        highest_level = max(members, key=lambda m: m.access_level).access_level
        return {
            self.server.user_from_project_member(m)
            for m in members
            if m.access_level == highest_level
        }

    @cached_property
    def dispatchers_and_why(self) -> Tuple[Set[User], str]:
        return self.server.users_from_people_spec(
            self.project_people_spec.dispatchers,
            repo=self,
            default=lambda: (self.owners, 'from project owners'),
        )

    @cached_property
    def dispatchers(self) -> Set[User]:
        return self.dispatchers_and_why[0]

    @cached_property
    def dispatchers_why(self) -> str:
        return self.dispatchers_and_why[1]

    @cached_property
    def issuewatchers_and_why(self) -> Tuple[Set[User], str]:
        return self.server.users_from_people_spec(
            self.project_people_spec.issuewatchers,
            repo=self,
            default=lambda: (set(), 'no one'),
        )

    @cached_property
    def issuewatchers(self) -> Set[User]:
        return self.issuewatchers_and_why[0]

    @cached_property
    def issuewatchers_why(self) -> str:
        return self.issuewatchers_and_why[1]

    @cached_property
    def required_approvers(self) -> Set[User]:
        return (
            {
                self.server.user_from_dict(user)
                for rule in self.approvalrules.list(all=True)
                if rule.approvals_required > 0
                for user in rule.eligible_approvers
            }
            if self.merge_requests_enabled
            else set()
        )

    @cached_property
    def mergeteamers_and_why(self) -> Tuple[Set[User], str]:
        return self.server.users_from_people_spec(
            self.project_people_spec.mergeteamers,
            repo=self,
            default=lambda: (self.required_approvers, 'from approval rules')
            if any(self.required_approvers)
            else (self.dispatchers, 'copied from dispatchers'),
        )

    @cached_property
    def mergeteamers(self) -> Set[User]:
        return self.mergeteamers_and_why[0]

    @cached_property
    def mergeteamers_why(self) -> str:
        return self.mergeteamers_and_why[1]

    @cached_property
    def printable_dispatchers(self) -> str:
        return ', '.join(sorted([d.username for d in self.dispatchers]))

    @cached_property
    def opened_mrs(self) -> List[MergeRequest]:
        return (
            [
                MergeRequest(self, mr)
                for mr in self.mergerequests.list(state='opened', all=True)
            ]
            if self.wrapped.merge_requests_enabled
            else []
        )

    def get_issue(self, iid: int) -> Issue:
        res = self.__issues.get(iid)
        if res is None:
            res = self.__add_raw_issue(self.issues.get(iid))
        return res

    def __add_raw_issue(self, raw_issue: gitlab_objects.Issue) -> Issue:
        res = Issue(self, raw_issue)
        self.__issues[res.iid] = res
        return res

    @cached_property
    def opened_issues(self) -> List[Issue]:
        return (
            [
                self.__add_raw_issue(issue)
                for issue in self.issues.list(state='opened', all=True)
            ]
            if self.wrapped.issues_enabled
            else []
        )

    @cached_property
    def printable_id(self) -> str:
        return self.wrapped.path_with_namespace

    @cached_property
    def public(self) -> bool:
        return self.wrapped.visibility == 'public'

    def register_people(self, spec: projects_spec.ProjectPeopleSpec) -> None:
        self.registered = True
        self.project_people_spec = spec.inherited_from(
            self.project_people_spec
        )

    def __repr__(self) -> str:
        return f'{type(self).__name__}({self.printable_id.__repr__()})'

    @cached_property
    def avatar_alt(self) -> str:
        return self.printable_id


class Group(Wrapper):
    def __init__(
        self,
        server: 'Server',
        group: Union[gitlab_objects.Group, gitlab_objects.GroupSubgroup],
    ):
        super().__init__(group)
        self.repos = [
            server.get_or_add_raw_repo(p)
            for p in self.wrapped.projects.list(all=True)
        ]

    def register_people(self, spec: projects_spec.ProjectPeopleSpec) -> None:
        for p in self.repos:
            p.register_people(spec)

    def __repr__(self) -> str:
        return f'{type(self).__name__}({self.full_path.__repr__()})'


TServer = TypeVar('TServer', bound='Server')


class Server(Wrapper):
    def __init__(self, gitlab: Gitlab, allow_private: bool = False):
        super().__init__(gitlab)
        self.__group_by_name: Dict[str, Group] = dict()
        self.__group_by_id: Dict[int, Group] = dict()
        self.__repo_by_name: Dict[str, Repository] = dict()
        self.__repo_by_id: Dict[int, Repository] = dict()
        self.users: Dict[str, User] = dict()
        self.allow_private = allow_private

    @classmethod
    def default(
        cls: Type[TServer],
        *,
        projects: projects_spec.ProjectRef = projects_spec.EMPTY_PROJECT_SPEC,
        private_token: Optional[str] = None,
        allow_private: bool = False,
    ) -> TServer:
        server = cls(
            Gitlab('https://www.gitlab.com', private_token=private_token),
            allow_private,
        )
        if private_token is not None:
            server.auth()
        server.register_project_spec(projects)
        return server

    @cached_property
    def registered_repositories(self) -> List[Repository]:
        return [
            repo
            for repo in self.__repo_by_id.values()
            if repo.registered and (repo.public or self.allow_private)
        ]

    def register_project_spec(self, spec: projects_spec.ProjectRef) -> None:
        if isinstance(spec, projects_spec.Project):
            repo = self.get_repo(spec)
            repo.register_people(spec)
        elif isinstance(spec, projects_spec.Group):
            group = self.get_group(spec)
            group.register_people(spec)
        elif isinstance(spec, projects_spec.List):
            for i in spec.items:
                self.register_project_spec(i.inherited_from(spec))
        else:
            raise ValueError(
                f'Unknown project spec kind {type(projects_spec)}'
            )

    def fetch_raw_project(self, id: int) -> gitlab_objects.Project:
        print(f'Fetching project {id}')
        return self.wrapped.projects.get(id)

    def fetch_raw_group(self, id: int) -> gitlab_objects.Group:
        print(f'Fetching group {id}')
        return self.wrapped.groups.get(id)

    def search_raw_group(self, name: str) -> gitlab_objects.Group:
        print(f'Searching group {name}')
        found = [
            s
            for s in self.wrapped.groups.list(search=name)
            if s.full_path == name
        ]
        assert len(found) == 1
        return found[0]

    def get_repo(self, ref: projects_spec.Ref) -> Repository:
        id = ref.id
        if isinstance(id, int):
            res = self.__repo_by_id.get(id)
            if res is None:
                res = self.__add_new_repo(self.fetch_raw_project(id))
        elif isinstance(id, str):
            res = self.__repo_by_name.get(id)
            if res is None:
                group_ref = projects_spec.Ref(get_repo_group(id))
                _group = self.get_group(group_ref)
                res = self.__repo_by_name[id]
        else:
            raise ValueError(f'Unknown project ref kind {type(id)}')
        return res

    def get_group(self, ref: projects_spec.Ref) -> Group:
        id = ref.id
        if isinstance(id, int):
            res = self.__group_by_id.get(id)
            if res is None:
                res = self.__add_new_group(self.fetch_raw_group(id))
        elif isinstance(id, str):
            res = self.__group_by_name.get(id)
            if res is None:
                res = self.__add_new_group(self.search_raw_group(id))
        else:
            raise ValueError(f'Unknown group ref kind {type(id)}')
        return res

    def __add_new_repo(
        self,
        gitlab_project: Union[
            gitlab_objects.Project, gitlab_objects.GroupProject
        ],
    ) -> Repository:
        res = Repository(self, gitlab_project)
        self.__repo_by_id[res.id] = res
        self.__repo_by_name[res.path_with_namespace] = res
        return res

    def get_or_add_raw_repo(
        self,
        gitlab_project: Union[
            gitlab_objects.Project, gitlab_objects.GroupProject
        ],
    ) -> Repository:
        res = self.__repo_by_id.get(gitlab_project.id)
        if res is None:
            res = self.__add_new_repo(gitlab_project)
        return res

    def __add_new_group(self, gitlab_group: gitlab_objects.Group) -> Group:
        res = Group(self, gitlab_group)
        self.__group_by_id[res.id] = res
        self.__group_by_name[res.full_path] = res
        return res

    def users_from_people_spec(
        self,
        spec: Optional[projects_spec.PeopleSpec],
        *,
        repo: Repository,
        default: Callable[[], Tuple[Set[User], str]],
    ) -> Tuple[Set[User], str]:
        if spec is None or isinstance(spec, projects_spec.Default):
            return default()
        elif isinstance(spec, projects_spec.Users):
            return (
                {
                    self.user_from_username(username)
                    for username in spec.usernames
                },
                'from hardcoded list',
            )
        elif isinstance(spec, projects_spec.Issue):
            issue = self.get_issue(repo=repo, ref=spec)
            issue.is_special = True
            return (issue.assignees, f'from issue {issue.printable_id}')
        else:
            raise ValueError(f'Unknown people spec kind {type(spec)}')

    def get_issue(self, *, repo: Repository, ref: projects_spec.Ref) -> Issue:
        id = ref.id
        if isinstance(id, int):
            return repo.get_issue(id)
        elif isinstance(id, str):
            reponame, issue_id = parse_issue_ref(id)
            return self.get_repo(projects_spec.Ref(reponame)).get_issue(
                issue_id
            )
        else:
            raise ValueError(f'Unknown issue reference kind {type(id)}')

    def user_from_username(self, username: str) -> User:
        res = self.users.get(username)
        if res is None:
            res = User(self, username)
            self.users[username] = res
        return res

    def user_from_dict(self, dic: dict) -> User:
        return self.user_from_username(dic['username']).update_from_dict(dic)

    def user_from_project_member(
        self, mem: gitlab_objects.ProjectMember
    ) -> User:
        return self.user_from_username(
            mem.username
        ).update_from_project_member(mem)

    def __repr__(self) -> str:
        return f'{type(self).__name__}({self.wrapped.__repr__()})'
