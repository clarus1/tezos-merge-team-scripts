from typing import Iterable, List, NamedTuple, Optional
import itertools
import action
import xgitlab


def collect_actions_from_mr(mr: xgitlab.MergeRequest,) -> List[action.Action]:
    print(f'Processing MR {mr.printable_id}', flush=True)

    if mr.merge_when_pipeline_succeeds:
        return [
            action.WaitForPipeline(mr, who, why)
            for who, why in mr.authors_and_merger.items()
        ]
    if mr.blocked:  # TODO: list blockers instead of boolean
        return [
            action.UnblockMR(mr, who, why) for who, why in mr.authors.items()
        ]
    if mr.work_in_progress:
        return [action.UnWIP(mr, who, why) for who, why in mr.authors.items()]
    res: List[action.Action] = []
    if mr.has_no_non_dispatcher_mergeteamers_reviewers:
        res += [
            action.AddMTReviewers(mr, who, why)
            for who, why in mr.authors.items()
        ]
        res += [
            action.HelpAddMTReviewers(mr, who) for who in mr.repo.dispatchers
        ]
    elif mr.has_less_than_2_reviewers:
        res += [
            action.AddReviewers(mr, who, why)
            for who, why in mr.authors.items()
        ]
        res += [
            action.HelpAddReviewers(mr, who) for who in mr.repo.dispatchers
        ]
    if mr.last_pipeline_failed:
        res += [
            action.FixCI(mr, who, why, mr.head_pipeline)
            for who, why in mr.authors_and_pipeline_user.items()
        ]
    if mr.has_uncompleted_tasks:
        res += [
            action.CompleteTask(mr, who, why)
            for who, why in mr.authors.items()
        ]
    for thread in mr.unresolved_threads:
        if (
            thread.last_comment_is_from_author_or_line_has_been_updated
            and thread.last_non_author_unresolved_comment is not None
        ):
            res += [
                action.CloseThreadOrReply(
                    mr,
                    thread.last_non_author_unresolved_comment.author,
                    thread.last_non_author_unresolved_comment,
                )
            ]
        elif thread.last_non_author_unresolved_comment is None:
            res += [
                action.CloseThreadOrReply(mr, author, comment)
                for author, comment in thread.unresolved_comments.items()
            ]
        else:
            res += [
                action.ReplyToThread(
                    mr, who, why, thread.last_non_author_unresolved_comment
                )
                for who, why in mr.authors.items()
            ]
    if not (mr.has_uncompleted_tasks or mr.has_unresolved_threads):
        if mr.last_pipeline_succeeded and mr.one_approval_left:
            if mr.has_conflicts:
                res += [
                    action.ManualRebase(mr, who, why)
                    for who, why in mr.authors.items()
                ]
            elif mr.has_diverged:
                res += [
                    action.Rebase(mr, who, why)
                    for who, why in mr.mergers.items()
                ]
            else:
                res += [
                    action.Merge(mr, who, why)
                    for who, why in mr.mergers.items()
                ]
        elif mr.last_finished_pipeline_succeeded:
            if mr.has_at_least_one_approval:
                res += [
                    action.ReviewOrApprove(mr, who, why)
                    for who, why in mr.mergeteam_reviewers.items()
                ]
            else:
                res += [
                    action.ReviewOrApprove(mr, who, why)
                    for who, why in mr.non_approvers_reviewers.items()
                ]
    if not any(res):
        res += [action.UnknownAction(mr, who) for who in mr.repo.dispatchers]
    return res


def collect_actions_from_issue(issue: xgitlab.Issue) -> List[action.Action]:
    print(f'Processing issue {issue.printable_id}', flush=True)

    # TODO: even special issues could generate actions
    if issue.is_special:
        return []

    # TODO: find dependencies

    if (not issue.is_assigned) and any(issue.repo.issuewatchers):
        return [
            action.AssignIssue(issue, who, 'is an issue watcher')
            for who in issue.repo.issuewatchers
        ]

    return [
        action.SolveIssue(issue, who, why) for who, why in issue.owners.items()
    ]


def collect_actions_from_repo(
    repo: xgitlab.Repository,
) -> Iterable[action.Action]:
    print(f'Processing repository {repo.printable_id}', flush=True)

    mr_actions = list(map(collect_actions_from_mr, repo.opened_mrs))
    issue_actions = list(map(collect_actions_from_issue, repo.opened_issues))

    return itertools.chain.from_iterable(
        itertools.chain(mr_actions, issue_actions)
    )


def collect_actions_from_server(
    server: xgitlab.Server,
) -> Iterable[action.Action]:
    return itertools.chain.from_iterable(
        map(collect_actions_from_repo, server.registered_repositories)
    )
