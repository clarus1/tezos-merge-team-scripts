from projects_spec import *

current_dispatchers = Users('raphael-proust')

current_issuewatchers = Users('vect0r', 'nfillion21')

PROJECTS = List(
    Group('tezos', dispatchers=current_dispatchers),
    Project('tezos/tezos', issuewatchers=current_issuewatchers),
    Group('nomadic-labs'),
    Project('metastatedev/tezos'),
)
